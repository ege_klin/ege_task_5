for number in range(10_000, 100_000):
    n1, n2, n3, n4, n5 = map(int, list(str(number)))
    sum1 = n1 + n3 + n5
    sum2 = n2 + n4
    if int(str(min(sum1, sum2)) + str(max(sum1, sum2))) == 723:
        print(number)
        break
